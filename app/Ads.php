<?php

namespace App;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;


class Ads extends Model implements HasMedia
{

    use HasMediaTrait;
    use Searchable;
    protected $fillable = [
        'title', 'address', 'location_id','section_id','user_id','price','specification'
    ];
}
