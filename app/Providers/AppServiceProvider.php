<?php

namespace App\Providers;
use App\Ads;
use App\Section;
use App\Location;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Composer;
use Auth;
use App\Message;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('includes.catagories',function($view){
            $ads= Ads::where('publish',1)->get();
            $sections = Section::get();
            foreach($sections as $section){
                $section->adsCount = $ads->where('section_id',$section->id)->count();
            }
            $view->with('section',$sections);
        
        });

        View::composer('includes.sideSearch',function($view){
            $location=Location::get();
            $sections = Section::get();
           
            $view->with(['location'=>$location,'sections'=>$sections]);
        
        });


        
            

        View::composer('includes.menu',function($view){
            if (Auth::user()) {
            $message=Message::where('seen',0)->where('to',Auth::user()->id)->get();
            $message=$message->count();
            
            $view->with('message',$message);
        }
        });
      

        



    }
}
