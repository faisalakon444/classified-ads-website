<?php

namespace App\Http\Controllers;
use App\Ads;
use App\Section;
use Illuminate\Http\Request;

class GuestController extends Controller
{
    public function index()
    {

       $ads=Ads::where('publish',1)->simplePaginate(3);
       foreach ($ads as $ad) 
       {
           $ad->getMedia();
           $ad->section=Section::where('id',$ad->section_id)->get('section_name');
       }

       $adsNew=Ads::where('publish',1)->latest('created_at')->take(6)->get();
       foreach ($adsNew as $ad) 
       {
           $ad->getMedia();
           $ad->section=Section::where('id',$ad->section_id)->get('section_name');
       }

       return view('welcome',['ads'=>$ads,'adsNew'=>$adsNew]);
       
    }
}
