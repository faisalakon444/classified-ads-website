<?php

namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use App\Location;
use App\Section;
use App\Ads;
use App\User;
use App\Like;
use App\Comment;
use App\Http\Requests\adsRequest;
use App\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class AdsController extends Controller
{

    




    public function create()

    {
        
        $sections=Section::get();
        $location=Location::get();
        return view('adsPost',['location'=>$location,'sections'=>$sections]);
    }




 

    public function post(adsRequest $request)
    {

      
        $ads = Ads::create([
            
            "title"=>$request->title,
            "section_id" =>$request->section_id,
            "specification"=>$request->specification,
            "price"=>$request->price,
            "location_id" =>$request->location,
            "user_id"=>auth()->user()->id,
            "address"=>$request->address,

        ]);

        $ads->addMultipleMediaFromRequest(["image"])->each->toMediaCollection();
        mail('faisal.code01@gmail.com', 'Hi faisal', 'New Ad Request');


        return redirect('/home');


    }




    public function show()
    {
        
        $ads=Ads::where('publish',1)->simplePaginate(3);
        foreach ($ads as $ad)
        {
            $ad->getFirstMedia();
            $ad->section=Section::where('id',$ad->section_id)->get('section_name');
            
        }
       
        
        return view('ads',['ads'=>$ads]);

    }




    public function edit($id)
    {

        $ads=Ads::find($id);
        $ads->getMedia();
        $ads->user= User::find($ads->user_id);
        $ads->section=Section::get();
        $ads->location=Location::get();
        $ads->sec=Section::where('id',$ads->section_id)->get('section_name');
        $ads->loca=Location::where('id',$ads->location_id)->get('location_name');

        return view('adsEdit',['ads'=>$ads]);

    } 



    public function delete($id)
    {
     
        $article=Ads::find($id);
        if ($article->user_id==auth()->user()->id) 
        {
            $article->delete();
        }
        
        return redirect()->back();


    }







    public function update(Request $request)
    {


        $ads=Ads::find($request->id);
        $ads->title=$request->title ? $request->title:$ads->title;
        $ads->section_id=$request->section_id ? $request->section_id:$ads->section_id;
        $ads->price=$request->price ? $request->price:$ads->price;
        $ads->location_id=$request->location_id ? $request->location_id:$ads->location_id;
        $ads->address=$request->address ? $request->address:$ads->address;
        $ads->specification=$request->specification ? $request->specification:$ads->specification;
        $ads->update();

        if (!empty($request->image)) 
        {
            $art=$ads->getMedia();
            $art[0]->delete();
            $ads->addMediaFromRequest('image')->toMediaCollection('default');
        } 

        return redirect('/userads');

        
    }











    public function search(Request $request)
    {
        
       
       
        if (isset($request->search)) 
        {
            $ads=Ads::where('title','LIKE',"%{$request->search}%")->get();
             
        }
        else 
        {
            $ads=Ads::get();
        }
        
        if ($request->has('location_id')) 
        {
            $ads=$ads->where('location_id',$request->location_id);
        }
        
        if ($request->has('section_id')) 
        {
            $ads=$ads->where('section_id',$request->section_id);
        }
        
        if ($request->has('price')) 
        {
            $ads=$ads->where('price','<',$request->price);
        }

       
        foreach ($ads as $ad) 
        {

            $ad->getFirstMedia();
            $ad->section=Section::where('id',$ad->section_id)->get('section_name');
            
        }

       
        return view('ads',['ads'=>$ads]);

    }





    public function sectionAds($id)
    {
        $ads=Ads::where('section_id',$id)->where('publish',1)->simplePaginate(6);

        foreach ($ads as $ad) 
        {

            $ad->getFirstMedia();
            $ad->section=Section::where('id',$ad->section_id)->get('section_name');
           
        }

        $section=Section::where('id',$id)->get('section_name');
       
        return view('ads',['ads'=>$ads,'section'=>$section]);

    }




    public function singleShow($id)
    {

        $ads=Ads::find($id);
        $ads->image=$ads->getMedia();
        $ads->user= User::find($ads->user_id);
        $ads->section=Section::where('id',$ads->section_id)->get();
        $ads->location=Location::where('id',$ads->location_id)->get();
        
        
        $comment=Comment::where('ads_id',$id)->simplePaginate(3);
        $userData=User::get(['id','name']);

        $adsAll=Ads::where('publish',1)->simplePaginate(3);
        foreach ($adsAll as $ad) 
        {
            $ad->getMedia();
            $ad->section=Section::where('id',$ad->section_id)->get('section_name');
            $ad->location=Location::where('id',$ad->location_id)->get('location_name');
        }

        return view('adsSingle',['ads'=>$ads,'comments'=>$comment,'userData'=>$userData,'adsAll'=>$adsAll]);

    }



    
    
    public function userAds()
    {

        $user=auth()->user()->id;
        $allads=Ads::where('user_id',$user)->simplePaginate(3);
        foreach($allads as $ads){
            $ads->image=$ads->getFirstMedia();
            $ads->section=Section::where('id',$ads->section_id)->get('section_name');
            $ads->location=Location::where('id',$ads->location_id)->get('location_name');
        }

        $adsAll=Ads::where('publish',1)->simplePaginate(3);
        foreach ($adsAll as $ad) {
            $ad->getFirstMedia();
            $ad->section=Section::where('id',$ad->section_id)->get('section_name');
            $ad->location=Location::where('id',$ad->location_id)->get('location_name');
        }
        

        return view('userAds',['ads'=>$allads,'adsAll'=>$adsAll]);

    }

   


   
    
    public function comment(Request $request)
    {

        $userId=auth()->user()->id;
        $comment=new Comment;
        $comment->comment=$request->comment;
        $comment->user_id=$userId;
        $comment->ads_id=$request->ads_id;
        $comment->save();
        return redirect()->back();
        
    }





    public function message(Request $request)
    {

        $userId=auth()->user()->id;
        $message=new Message;
        $message->message=$request->message;
        $message->from=$userId;
        $message->to=$request->user_id;
        $message->ads_id=$request->ads_id;
        $message->save();
        return redirect()->back();
        
    }




    public function messageShow()
    {

        $messages=Message::where('to',auth()->user()->id)->get();
        foreach ($messages as $message) 
        {
            $message->sender=User::where('id',$message->from)->get();
        }
       
        return view('message',['m'=>$messages]);
    }





    public function deleteMessage($id)
    {
     
        
        $message=Message::find($id);
        if ($message->to==auth()->user()->id) 
        {
            $message->delete();
        }
        
        return redirect()->back();
    }
    

}
