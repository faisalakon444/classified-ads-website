<?php

namespace App\Http\Controllers;
use App\User;
use App\Ads;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {

        $admin=auth()->user()->id;
        $useres=User::with('roles')->where('id', '!=',$admin)->get();
        foreach ($useres as $user) 
        {
            $user->getMedia();
        }
        $ads=Ads::where('publish',0)->simplepaginate(6);
     
        foreach ($ads as $ad) 
        {
            $ad->getFirstMedia();
        }

        return view('admin',['useres'=>$useres,'ads'=>$ads]);

    }


// ------------------------------------------

    public function publishedAd()
    {
        
        $ads=Ads::where('publish',1)->simplepaginate(6);
     
        foreach ($ads as $ad) 
        {
            $ad->getFirstMedia();
        }

        return view('published',['ads'=>$ads]);

    }


// --------------------------------------------------
    public function CanPost(Request $request)
    { 
      
        $user=User::find($request['user_id']);
        $user->syncRoles('advertiser');
        
        
    }

// ---------------------------------------------------

    public function CanNotPost(Request $request)
    { 
      
        $user=User::find($request['user_id']);
        $user->syncRoles('user');
        
        
    }
// -----------------------------------------------------------
    public function publish(Request $request)
    {


        $article=Ads::find($request->ads_id);
        $article->publish=$request->publish;
        $article->update();
        $userEmail=User::find($article->user_id);
        if ($request->publish==1) 
        {
            mail($userEmail->email, 'Hi '.$userEmail->name, 'Your ad has published');
        } 
        else 
        {
            mail($userEmail->email, 'Hi '.$userEmail->name, 'Your ad has removed');
        }
        
        
    
        return redirect()->back();

    }

    

   
}
