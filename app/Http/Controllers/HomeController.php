<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Blood;
use App\Gender;
use App\Location;
use App\User;
use App\Ads;
use Illuminate\Support\Facades\View;
use App\Section;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        
        $ads=Ads::where('publish',1)->simplePaginate(3);
        foreach ($ads as $ad) 
        {
            $ad->getMedia();
            $ad->section=Section::where('id',$ad->section_id)->get('section_name');
        }

        $adsNew=Ads::where('publish',1)->latest('created_at')->take(6)->get();
        foreach ($adsNew as $ad) 
        {
            $ad->getMedia();
            $ad->section=Section::where('id',$ad->section_id)->get('section_name');
        }

        return view('home',['ads'=>$ads,'adsNew'=>$adsNew]);

    }




    public function create()
    {
        $user=auth()->user();
        $blood=Blood::get();
        $gender=Gender::get();
        $location=Location::get();
        
        
        return view('profileCreate',['user'=>$user,'gender'=>$gender,'location'=>$location,'bloods'=>$blood]);
    }




    public function update(Request $request)
    {
        
        $user=auth()->user();
        $user->name=$request->name ? $request->name:$user->name;
        $user->email=$request->email ? $request->email:$user->email;
        $user->address=$request->address ? $request->address:$user->address;
        $user->gender=$request->gender ? $request->gender:$user->gender;
        $user->blood_id=$request->blood_id ? $request->blood_id:$user->blood_id;
        $user->location=$request->location ? $request->location:$user->location;
        $user->phone_number=$request->phone_number ? $request->phone_number:$user->phone_number;
        $user->update();
        if (!empty($request->image)) 
        {
            $art=$user->getMedia();
            if (isset($art)) {
                $art[0]->delete();
            }
            
            $user->addMediaFromRequest('image')->toMediaCollection();
        } 

        
         
         return redirect('/home');
    }



    
    public function show()
    {
        $user=auth()->user();
        $user->getMedia('default');
 
        return view('profile',['user'=>$user]);
    }





}
