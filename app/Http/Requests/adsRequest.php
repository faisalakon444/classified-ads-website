<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class adsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'section_id' => 'required',
            'specification' => 'max:1000',
            'price' => 'required|integer',
            'location' =>'required',
        ];
    }

    public function messages()
    {
        $messages = [

            'title.required'  => 'title Field Is Required',
            'title.max'  => 'too long',
            'section_id.required' => 'Please select section',
            


            

        ];

        return $messages;
    }
}
