<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'GuestController@index')->name('guest.home');
Auth::routes();




Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profile', 'HomeController@show')->name('profile.show');
Route::get('/profile-update', 'HomeController@create')->name('profile.create');
Route::post('/profile-update', 'HomeController@update')->name('profile.update');







Route::get('/ads-create', 'adsController@create')->name('ads.create')->middleware('role_or_permission:advertiser|ads_post');
Route::post('/ads-create', 'adsController@post')->name('ads.post')->middleware('role_or_permission:advertiser|ads_post');
Route::get('/ads', 'adsController@show')->name('ads.show');
Route::get('/adsdetails{id}', 'adsController@singleShow')->name('ads.single');
Route::get('/userads', 'adsController@userAds')->name('ads.user')->middleware('role_or_permission:advertiser|ads_post');
Route::get('/ads-edit{id}', 'adsController@edit')->name('ads.edit');
Route::get('/ads-delete{id}', 'AdsController@delete')->name('ads.delete');
Route::get('/message-delete{id}', 'AdsController@deleteMessage')->name('message.delete')->middleware('role_or_permission:advertiser|ads_post');
Route::get('/sectionads{id}', 'AdsController@sectionAds')->name('ads.section');
Route::post('/search', 'adsController@search')->name('search');
Route::post('/ads-update', 'adsController@update')->name('ads.update')->middleware('role_or_permission:advertiser|ads_post');
Route::post('/comment', 'AdsController@comment')->name('ads.comment');
Route::post('/message', 'AdsController@message')->name('message');
Route::get('/messageshow', 'AdsController@messageShow')->name('message.show')->middleware('role_or_permission:advertiser|ads_post');







Route::get('/admin-panel', 'AdminController@index')->name('admin.index')->middleware('role_or_permission:admin|ads_post');
Route::post('/canPost', 'adminController@canPost')->name('canPost')->middleware('role_or_permission:admin|ads_post');
Route::post('/canNpost', 'adminController@canNotPost')->name('canNotPost')->middleware('role_or_permission:admin|ads_post');
Route::post('/publish', 'AdminController@publish')->name('ads.publish')->middleware('role_or_permission:admin|ads_post');
Route::get('/publishedad', 'AdminController@publishedAd')->name('ads.published')->middleware('role_or_permission:admin|ads_post');
























