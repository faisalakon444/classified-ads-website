<div class="site-mobile-menu">
	<div class="site-mobile-menu-header">
	  <div class="site-mobile-menu-close mt-3">
		<span class="icon-close2 js-menu-toggle"></span>
	  </div>
	</div>
	<div class="site-mobile-menu-body"></div>
  </div>
  <div class="site-wrap">

    
    
    <header class="site-navbar container py-0 bg-white" role="banner">

      <!-- <div class="container"> -->
        <div class="row align-items-center">
          
          <div class="col-6 col-xl-2">
          <h1 class="mb-0 site-logo"><a href="{{url('/')}}" class="text-black mb-0">Classy<span class="text-primary">Ads</span>  </a></h1>
          </div>
          <div class="col-12 col-md-10 d-none d-xl-block">
            <nav class="site-navigation position-relative text-right" role="navigation">

              <ul class="site-menu js-clone-nav mr-auto d-none d-lg-block">
                @if (Auth::user())
              <li><a href="{{route('home')}}">Home</a></li>
              @endif
              <li><a href="{{route('ads.show')}}">Ads</a></li>
              @if (Auth::user())
              @role('advertiser')
              <li><a href="{{route('ads.user')}}">My Ads</a></li>
              @endrole
              @endif
               
               
                @role('advertiser')
                @if (isset($message))
              <li><a  href="{{route('message.show')}}">Message <span class="text-warning">{{$message}}</span></a></li>
                @endif
                @endrole
				@if (Auth::guest())
			<li class="ml-xl-3 login"><a href="{{route('login')}}"><span class="border-left pl-xl-4"></span>Log In</a></li>
				<li><a href="{{url("/register")}}">Register</a></li>	
				@else
				<li class="has-children">
					<a><i class="fa fa-user"></i> {{auth::user()->name}}</a>
					<ul class="dropdown">
          <li><a href="{{url('/profile')}}">My Profile</a></li>
           
            @role('admin')
          <li class="nav-item">
          <a href="{{route('admin.index')}}">Admin Panel</a>
          </li>
          @endrole
          <li><a href="{{route('profile.create')}}">Profile Update</a></li>
					  <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
						document.getElementById('logout-form').submit();">Logout</a></li>
						<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
							@csrf
						</form> </li>
					
					</ul>
				  </li>
				@endif
              
        @if (Auth::user())
        @role('advertiser')
        <li><a href="{{route('ads.create')}}" class="cta"><span class="bg-primary text-white rounded">+ Post an Ad</span></a></li>
        @endrole
        @endif

              </ul>
            </nav>
          </div>
         

          <div class="d-inline-block d-xl-none ml-auto py-3 col-6 text-right" style="position: relative; top: 3px;">
            <a href="#" class="site-menu-toggle js-menu-toggle text-black"><span class="icon-menu h3"></span></a>
          </div>

        </div>
      <!-- </div> -->
      
    </header>
