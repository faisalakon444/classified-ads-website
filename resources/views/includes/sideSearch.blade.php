{{-- <div class="col-lg-3 ml-auto"> --}}

    <div class="mb-5">
      <h3 class="h5 text-black mb-3">Filters</h3>
    <form action="{{route('search')}}" method="post">
        @csrf
        <div class="form-group">
          <input name="search" type="text" placeholder="What are you looking for?" class="form-control">
        </div>

        <div class="row form-group">
        
          <div class="col-md-12">
            <label class="text-black" for="email">Section</label> 
            <select class="form-control" name="section_id">
              <option selected disabled>Select your product section</option>
              @foreach ($sections as $item)
              <option value="{{$item->id}}">{{$item->section_name}}</option>
                @endforeach
           
          </select>

          </div>
      </div>
      <div class="row form-group">
        
        <div class="col-md-12">
          <label class="text-black" for="email">Location</label> 
          <select class="form-control" name="location_id">
            <option selected disabled>Select your product location</option>
            @foreach ($location as $item)
          <option value="{{$item->id}}">{{$item->location_name}}</option>
            @endforeach
            
            {{-- <option value="">HOME APLIENCE</option>
            <option value="">VEHICLES</option> --}}
         
        </select>

        </div>
    </div>
   
    <div class="mb-5">
      
        <div class="form-group">
          <p>Price</p>
        </div>
        
        <div class="slidecontainer">
         
          <input style="content:BDT" name="price" type="range"  max="2000000" value="50" class="slider" >
          
          
        </div>
    </div>
        
        <button type="submit">Search</button>
      </form>
    </div>

  {{-- </div> --}}
