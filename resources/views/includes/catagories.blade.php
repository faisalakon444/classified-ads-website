<div class="overlap-category mb-5">
    <div class="row align-items-stretch no-gutters">

      @foreach ($section as $item)
      <div class="col-sm-6 col-md-4 mb-4 mb-lg-0 col-lg-2">
        <a href="{{url('/sectionads'.$item['id'])}}" class="popular-category h-100">
          {{-- <span class="icon"><span class="flaticon-house"></span></span> --}}
          <span class="caption mb-2 d-block">{{$item->section_name}}</span>
        <span class="number">{{$item->adsCount}}</span>
        </a>
      </div> 
      @endforeach
     
     
    </div>
  </div>