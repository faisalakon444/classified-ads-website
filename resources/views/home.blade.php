
     
@extends('master')
@section('menubottom')
   @include('includes.menubottom')
@endsection
@section('content')
 


   <div class="site-section bg-light">
     <div class="container">

      
       @include('includes.catagories')
       
       <div class="row">
         <div class="col-12">
           <h2 style="font-weight:bold"  class="h5 mb-4 text-black">Latest Ads</h2>
         </div>
       </div>
       <div class="row">
         <div class="col-12  block-13">
           <div class="owl-carousel nonloop-block-13">
             @foreach ($adsNew as $item)
             <div class="d-block d-md-flex listing vertical">
               <a href="{{url('/adsdetails'.$item['id'])}}" class="img d-block" style="background-image:url('{{$item['media'][0]->getUrl()}}')"></a>

               <div class="lh-content">
                 <span class="category">{{$item['section'][0]->section_name}}</span>
                 <h3><a style="color:#30E3CA">{{$item->title}}</a></h3>
                   <address>{{$item->address}}</address>
                   <p class="mb-0">
                   <span class="text-warning">{{$item->price}} BDT</span>
                     
                   </p>
                 
               </div>
             </div>
             @endforeach

           </div>
         </div>


       </div>
     </div>
   </div>
   



   <div class="site-section">
     <div class="container">
       <div class="row">
         <div class="col-12">
           <h2 style="font-weight:bold" class="h5 mb-4 text-black">All Ads</h2>
         </div>
         <div class="col-lg-12">
 
           <div class="row">
               @foreach ($ads as $item)
               
             <div  class="col-lg-4">
                 
               <div class="d-block d-md-flex listing vertical">
               <a href="{{url('/adsdetails'.$item['id'])}}" class="img d-block" style="background-image:url('{{$item['media'][0]->getUrl()}}')"></a>
                 <div  class="lh-content">
                   <span class="category">{{$item['section'][0]->section_name}}</span>
                  
                   <h3><a style="color:#30E3CA">{{$item->title}}</a></h3>
                   <address>{{$item->address}}</address>
                   <p class="mb-0">
                   <span class="text-warning">{{$item->price}} BDT</span>
                     
                   </p>
                 </div>
             
               </div>
         
             </div>
         
             @endforeach
            
             
 
           </div>
 
 
 
 
 
 
 
 
 
 
 
 
           <div class="col-12 mt-5 text-center">
            
               {{ $ads->links() }}
            
           </div>
 
         </div>
         

       </div>
     </div>
   </div>
 



      

   @endsection
   