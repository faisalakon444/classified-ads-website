
@extends('master')

@section('content')
  

<div class="site-blocks-cover inner-page-cover overlay" style="background-image: url(images/hero_1.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
    <div class="container">
      <div class="row align-items-center justify-content-center text-center">

        <div class="col-md-10" data-aos="fade-up" data-aos-delay="400">
          
          
          <div class="row justify-content-center mt-5">
            <div class="col-md-8 text-center">
              <h1>Profile Update</h1>
            </div>
          </div>

          
        </div>
      </div>
    </div>
  </div>
  <div class="site-section bg-light">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-7 mb-5"  data-aos="fade">

          

          <form method="POST" action="{{ route('profile.update') }}" class="p-5 bg-white" enctype="multipart/form-data">
            @csrf

            <div class="row form-group">
              
                <div class="col-md-12">
                  <label class="text-black" for="email">Name</label> 
                  <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $user['name'] }}" required autocomplete="name" autofocus>
  
                  @error('name')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
                </div>
              </div>

              <div class="row form-group">
              
                <div class="col-md-12">
                  <label class="text-black" for="email">Email</label> 
                  <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{$user['email']}}" required autocomplete="email">

                  @error('email')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
                </div>
            </div>

            <div class="row form-group">
              
                <div class="col-md-12">
                  <label class="text-black" for="email">Image</label> 
                  <input id="name" type="file" class="form-control" name="image">
                </div>
            </div>

            <div class="row form-group">
              
                <div class="col-md-12">
                  <label class="text-black" >Phone Number</label> 
                <input type="string" class="form-control" name="phone_number" value="{{$user['phone_number']}}">

                  @error('phone_number')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
                </div>
            </div>
           
            
           
            <div class="row form-group">
              
                <div class="col-md-12">
                  <label class="text-black">Gender</label> 
                
                <select class="form-control" name="gender">
                  @foreach($gender as $sex)
                  @if ($user['gender']==$sex['id'])
                <option selected disabled>{{$sex->sex}}</option>
                  @endif
                  @endforeach
                  <option disabled>Select your gender</option>
          
                    @foreach($gender as $sex)
                    <option value="{{$sex->id}}">{{$sex->sex}}</option>
                    @endforeach
                </select>

                </div>
            </div>

            <div class="row form-group">
              
                <div class="col-md-12">
                  <label class="text-black" for="email">Blood Group</label> 
                  <select class="form-control" name="blood_id">
                  @foreach($bloods as $blood)
                  @if ($user['blood_id']==$blood['id'])
                   <option selected disabled>{{$blood->groups}}</option>
                  @endif
                  @endforeach
                    
                  <option disabled>Select your blood group</option>
                    @foreach($bloods as $blood)
                    <option value="{{$blood->id}}">{{$blood->groups}}</option>
                    @endforeach
                </select>

                </div>
            </div>

            <div class="row form-group">
              
                <div class="col-md-12">
                  <label class="text-black" for="email">Address</label> 
                <input type="text" class="form-control" name="address" value="{{$user['address']}}">

                </div>
            </div>

            <div class="row form-group">
              
                <div class="col-md-12">
                  <label class="text-black" for="email">Location</label> 
                  <select class="form-control" name="location">
                    @foreach($location as $name)
                    @if ($user['location']==$name['id'])
                     <option selected disabled>{{$name->location_name}}</option>
                    @endif
                    @endforeach
                    <option disabled>Select your location</option>
                    @foreach($location as $name)
                    <option value="{{$name->id}}">{{$name->location_name}}</option>
                    @endforeach
                </select>

                </div>
            </div>


            <div class="row form-group">
              <div class="col-md-12">
                <input type="submit" value="Update" class="btn btn-primary py-2 px-4 text-white">
              </div>
            </div>

           
           
           
          </form>
        </div>
        
      </div>
    </div>
  </div>

  <div class="newsletter bg-primary py-5">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-6">
          <h2>Newsletter</h2>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
        </div>
        <div class="col-md-6">
          
          <form class="d-flex">
            <input type="text" class="form-control" placeholder="Email">
            <input type="submit" value="Subscribe" class="btn btn-white"> 
          </form>
        </div>
      </div>
    </div>
  </div>
  @endsection
