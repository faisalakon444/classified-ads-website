
@extends('master')

@section('content')
  

<div class="site-blocks-cover inner-page-cover overlay" style="background-image: url(images/hero_1.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
    <div class="container">
      <div class="row align-items-center justify-content-center text-center">

        <div class="col-md-10" data-aos="fade-up" data-aos-delay="400">
          
          
          <div class="row justify-content-center mt-5">
            <div class="col-md-8 text-center">
              <h1>Edit Ads</h1>
            </div>
          </div>

          
        </div>
      </div>
    </div>
  </div>
  <div class="site-section bg-light">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-7 mb-5"  data-aos="fade">

          

          <form method="POST" action="{{ route('ads.update') }}" class="p-5 bg-white" enctype="multipart/form-data">
            @csrf
            <input type="text" hidden name="id" value="{{$ads->id}}">
            <div class="row form-group">
              
                <div class="col-md-12">
                  <label class="text-black">Title</label> 
                <input type="text" name="title" class="form-control" value="{{$ads->title}}">
  
                  @error('title')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
                </div>
            </div>

            <div class="row form-group">
              
                <div class="col-md-12">
                  <label class="text-black" for="email">Section</label> 
                  <select class="form-control" name="section_id">
                    @if (isset($ads['sec']->section_name))
                  <option selected disabled>{{$ads['sec']->section_name}}</option>
                    @endif
                    <option disabled>Select your product section</option>
                    @foreach($ads['section'] as $section)
                    <option value="{{$section->id}}">{{$section->section_name}}</option>
                    @endforeach
                </select>

                </div>
            </div>

            <div class="row form-group">
              
                <div class="col-md-12">
                  <label class="text-black">Specification</label> 
                <input type="text" class="form-control" name="specification" value="{{$ads->specification}}">

                  @error('specification')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
                </div>
            </div>

            <div class="row form-group">
              
                <div class="col-md-12">
                  <label class="text-black" for="email">Image</label> 
                  <input id="name" type="file" class="form-control" name="image[]" multiple>
                </div>
            </div>

            <div class="row form-group">
              
                <div class="col-md-12">
                  <label class="text-black" >Price</label> 
                <input type="string" class="form-control" name="price" value="{{$ads->price}}">

                  @error('price')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
                </div>
            </div>
           


            <div class="row form-group">
              
                <div class="col-md-12">
                  <label class="text-black" for="email">Address</label> 
                <input type="text" class="form-control" name="address" value="{{$ads->address}}">

                </div>
            </div>

            <div class="row form-group">
              
                <div class="col-md-12">
                  <label class="text-black" for="email">Location</label> 
                  <select class="form-control" name="location">
                      @if (isset($ads['loca']->location_name))
                      <option selected disabled>{{$ads['loca']->location_name}}</option> 
                      @endif
                    <option disabled>Select your location</option>
                    @foreach($ads['location'] as $name)
                    <option value="{{$name->id}}">{{$name->location_name}}</option>
                    @endforeach
                </select>

                </div>
            </div>


            <div class="row form-group">
              <div class="col-md-12">
                <input type="submit" value="Post" class="btn btn-primary py-2 px-4 text-white">
              </div>
            </div>

           
           
           
          </form>
        </div>
        
      </div>
    </div>
  </div>

  <div class="newsletter bg-primary py-5">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-6">
          <h2>Newsletter</h2>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
        </div>
        <div class="col-md-6">
          
          <form class="d-flex">
            <input type="text" class="form-control" placeholder="Email">
            <input type="submit" value="Subscribe" class="btn btn-white"> 
          </form>
        </div>
      </div>
    </div>
  </div>
  @endsection
