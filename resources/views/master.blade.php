@include('includes.head')
<body>
	@include('includes.menu')
    @yield('menubottom')
    @yield('content')

    @include('includes.footer')

