
@extends('master')

@section('content')
  

<div class="site-blocks-cover inner-page-cover overlay" style="background-image: url(images/hero_1.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
    <div class="container">
      <div class="row align-items-center justify-content-center text-center">

        <div class="col-md-10" data-aos="fade-up" data-aos-delay="400">
          
          
          <div class="row justify-content-center mt-5">
            <div class="col-md-8 text-center">
              <h1>Ads Post</h1>
            </div>
          </div>

          
        </div>
      </div>
    </div>
  </div>
  <div class="site-section bg-light">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-7 mb-5"  data-aos="fade">

          

          <form method="POST" action="{{ route('ads.post') }}" class="p-5 bg-white" enctype="multipart/form-data">
            @csrf

            <div class="row form-group">
              
                <div class="col-md-12">
                  <label class="text-black">Title</label> 
                  <input type="text" name="title" class="form-control" required autofocus>
  
                  @error('title')
                      {{-- <span class="invalid-feedback" role="alert"> --}}
                          <strong class="text-warning">{{ $message }}</strong>
                      {{-- </span> --}}
                  @enderror
                </div>
            </div>

            <div class="row form-group">
              
                <div class="col-md-12">
                  <label class="text-black" for="email">Section</label> 
                  <select class="form-control" name="section_id">
                  <option selected disabled>Select your product section</option>
                    @foreach($sections as $section)
                    <option value="{{$section->id}}">{{$section->section_name}}</option>
                    @endforeach
                </select>

                @error('section_id')
                {{-- <span class="invalid-feedback" role="alert"> --}}
                    <strong class="text-warning">{{ $message }}</strong>
                {{-- </span> --}}
            @enderror

                </div>
            </div>

            <div class="row form-group">
              
                <div class="col-md-12">
                  <label class="text-black">Specification</label> 
                  <input type="text" class="form-control" name="specification">

                  @error('specification')
                      {{-- <span class="invalid-feedback" role="alert"> --}}
                          <strong class="text-warning">{{ $message }}</strong>
                      {{-- </span> --}}
                  @enderror
                </div>
            </div>

            <div class="row form-group">
              
                <div class="col-md-12">
                  <label class="text-black" for="email">Image</label> 
                  <input id="name" type="file" class="form-control" name="image[]" multiple>
                </div>
            </div>

            <div class="row form-group">
              
                <div class="col-md-12">
                  <label class="text-black" >Price</label> 
                  <input type="string" class="form-control" name="price">

                  @error('price')
                      {{-- <span class="invalid-feedback" role="alert"> --}}
                          <strong class="text-warning">{{ $message }}</strong>
                      {{-- </span> --}}
                  @enderror
                </div>
            </div>
           


            <div class="row form-group">
              
                <div class="col-md-12">
                  <label class="text-black" for="email">Address</label> 
                <input type="text" class="form-control" name="address">

                </div>
            </div>

            <div class="row form-group">
              
                <div class="col-md-12">
                  <label class="text-black">Location</label> 
                  <select class="form-control" name="location">
                    <option selected disabled>Select your location</option>
                    @foreach($location as $loc)
                    <option value="{{$loc->id}}">{{$loc->location_name}}</option>
                    @endforeach
                </select>
                @error('location')
                {{-- <span class="invalid-feedback" role="alert"> --}}
                    <strong class="text-warning">{{ $message }}</strong>
                {{-- </span> --}}
            @enderror
                </div>
            </div>


            <div class="row form-group">
              <div class="col-md-12">
                <input type="submit" value="Post" class="btn btn-primary py-2 px-4 text-white">
              </div>
            </div>

           
           
           
          </form>
        </div>
        
      </div>
    </div>
  </div>

  <div class="newsletter bg-primary py-5">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-6">
          <h2>Newsletter</h2>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
        </div>
        <div class="col-md-6">
          
          <form class="d-flex">
            <input type="text" class="form-control" placeholder="Email">
            <input type="submit" value="Subscribe" class="btn btn-white"> 
          </form>
        </div>
      </div>
    </div>
  </div>
  @endsection
