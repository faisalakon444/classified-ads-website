
@extends('master')

@section('content')
  



<div class="site-blocks-cover inner-page-cover overlay" style="background-image: url(images/hero_1.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
  <div class="container">
    <div class="row align-items-center justify-content-center text-center">

      <div class="col-md-10" data-aos="fade-up" data-aos-delay="400">
        
        
        <div class="row justify-content-center mt-5">
          <div class="col-md-8 text-center">
            <h1>Ads Details</h1>
            {{-- <p class="mb-0">Don St, Brooklyn, New York</p> --}}
          </div>
        </div>

        
      </div>
    </div>
  </div>
</div>  

<div class="site-section">
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
        
        <div class="mb-4">
          <div class="slide-one-item home-slider owl-carousel">
            @foreach ($ads['image'] as $item)
            <div><img style="width:800px; height:600px" src="{{$item->getUrl()}}" alt="Image" class="img-fluid"></div>
            @endforeach
           
          </div>
          <div class="d-block d-md-flex listing vertical">
              <div class="lh-content">
               
                <span class="category">{{$ads['section'][0]->section_name}}</span>
                <h3><a style="color:#30E3CA">{{$ads->title}}</a></h3>
                
                <address>{{$ads->address}},{{$ads['location'][0]->location_name}}</address>
                <p class="mb-0">
                <span class="text-warning">{{$ads->price}} BDT</span>
                
                <p>{{$ads->specification}}</p>                
              @if (auth::user())
              <button name="submit" onclick="comment_f()" type="button" class="btn btn-primary text-white bold rounded">Comment</button>
              <button name="submit" onclick="message_f()" type="button" class="btn btn-warning text-white bold rounded">Message</button>

              @endif
      
              </div>
              @if (auth::user())
              <div style="display:none; padding-top:30px;" id="comid" class="card" >
                                           
                <textarea style="border-color:black;" id="myText"  class="form-control"  name="comment" type="text"></textarea>
                <button onclick="comment(document.getElementById('myText').value,'{{$ads['id']}}', '{{Auth::user()->id}}')" name="submit" class="btn btn-submit" value="Submit"> submit</button>
            </div>
            <div style="display:none; padding-top:30px;" id="mesid" class="card" >
                                           
              <textarea style="border-color:black;" id="myTextmessage"  class="form-control"  name="comment" type="text"></textarea>
              <button onclick="message(document.getElementById('myTextmessage').value,'{{$ads['id']}}', '{{$ads['user_id']}}')" name="submit" class="btn btn-submit" value="Submit"> submit</button>
          </div>
              @endif
              
            </div>
        
        </div>
        
        @foreach ($comments as $comment)
        <div class="d-block d-md-flex listing vertical">
          <!-- <a href="#" class="img d-block" style="background-image: url('images/img_2.jpg')"></a> -->
          <div class="lh-content">                           
                @foreach ($userData as $item)
                @if ($item['id']==$comment['user_id'])
                
                <span class="category">{{$item->name}}</span>

                @endif 
                @endforeach
                <div><p>{{$comment->comment}}</p> </div>
              </div>

            </div>
      
        @endforeach
        
      </div>

      
     
      <div class="col-lg-3 ml-auto">
        
        <div class="mb-5">
          
          <h3 style="font-weight:bold" class="h5 text-black mb-3">Advertiser</h3>
          <hr>
        <span class="icon-user"> Name: <span style="color: green;">{{$ads['user']->name}}</span></span>
          <hr>
        <span class="icon-phone"> Contact: <span style="color: green;">{{$ads['user']->phone_number}}</span> </span>
          <hr>
          
        </div>
       
        @include('includes.sideSearch')
      </div>
     
    </div>
  </div>
</div>








<div class="site-section">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2 style="font-weight:bold" class="h5 mb-4 text-black">All Ads</h2>
      </div>
      <div class="col-lg-12">

        <div class="row">
            @foreach ($adsAll as $item)
            
          <div  class="col-lg-4">
              
            <div class="d-block d-md-flex listing vertical">
            <a href="{{url('/adsdetails'.$item['id'])}}" class="img d-block" style="background-image:url('{{$item['media'][0]->getUrl()}}')"></a>
              <div  class="lh-content">
                <span class="category">{{$item['section'][0]->section_name}}</span>
                
                <h3><a style="color:#30E3CA">{{$item->title}}</a></h3>
                <address>{{$item->address}},{{$item['location'][0]->location_name}}</address>
                <p class="mb-0">
                <span class="text-warning">{{$item->price}} BDT</span>
                  
                </p>
              </div>
          
            </div>
      
          </div>
      
          @endforeach
         
          

        </div>



        <div class="col-12 mt-5 text-center">
         
            {{ $adsAll->links() }}
         
        </div>

      </div>
      

    </div>
  </div>
</div>
















<script>
  function comment_f() {
 
 $("#comid").toggle();

}

function message_f() {
 
 $("#mesid").toggle();

}

function message(i,a,b){
 $.ajax({
 method: 'POST', // Type of response and matches what we said in the route
 url: '/message', // This is the url we gave in the route
 data: {"_token": "{{ csrf_token() }}",'ads_id':a,'user_id':b,'message':i}, // a JSON object to send back
 success: function() {   
     location.reload();  
 },
 error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
     console.log(JSON.stringify(jqXHR));
     console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
 }
});

}


function comment(i,a,b){
  console.log(i);
 $.ajax({
 method: 'POST', // Type of response and matches what we said in the route
 url: '/comment', // This is the url we gave in the route
 data: {"_token": "{{ csrf_token() }}",'ads_id':a,'user_id':b,'comment':i}, // a JSON object to send back
 success: function() {   
     location.reload();  
 },
 error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
     console.log(JSON.stringify(jqXHR));
     console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
 }
});

}
</script>






    @endsection
    