
@extends('master')

@section('content')
  

<div class="site-blocks-cover inner-page-cover overlay" style="background-image: url(images/hero_1.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
    <div class="container">
      <div class="row align-items-center justify-content-center text-center">

        <div class="col-md-10" data-aos="fade-up" data-aos-delay="400">
          
          
          <div class="row justify-content-center mt-5">
            <div class="col-md-8 text-center">
              <h1>Ads Listings</h1>
              <p class="mb-0">Choose product you want</p>
            </div>
          </div>

          
        </div>
      </div>
    </div>
  </div>  

  <div class="site-section">
    <div class="container">
      <div class="row">
        <div class="col-lg-8">

          <div class="row">
            <div class="col-12">
        @if (isset($section))
            <h2 style="font-weight:bold"  class="h5 mb-4 text-black">Section: <span class="text-warning">{{$section[0]->section_name}}</span></h2>
        @endif
        @if ($ads->count()==0)
        <h2 style="font-weight:bold"  class="h5 mb-4 text-black"><span class="text-warning">No ads found</span></h2>

        @endif
       
              
            </div>
              @foreach ($ads as $item)
             
              
            <div  class="col-lg-6">
                
              <div class="d-block d-md-flex listing vertical">
              <a href="{{url('/adsdetails'.$item['id'])}}" class="img d-block" style="background-image:url('{{$item['media'][0]->getUrl()}}')"></a>
              
             
                <div  class="lh-content">
                  <span class="category">{{$item['section'][0]->section_name}}</span>
                  {{-- @if ($item['isLiked']>0) --}}
                  {{-- <a name="submit" id="like_button" class="btn" value=""  onclick="likes('{{$postData['id']}}',1)">{{$postData['like_count']}} <i class="fa fa-heart"></i>UnLike</a> --}}
                  {{-- <a name="submit" onclick="likes('{{$item['id']}}',1)" class="bookmark"><span class="icon-heart"></span></a> --}}
                  {{-- @else --}}
                  {{-- <a name="submit" class="btn " value=""  onclick="likes('{{$postData['id']}}',2)">{{$postData['like_count']}} <i class="fa fa-heart"></i> Like</a> --}}
                  {{-- <a name="submit" onclick="likes('{{$item['id']}}',2)" class="bookmark"><span style="color:red;"class="icon-heart"></span></a> --}}
                  {{-- @endif --}}

                  
                  <h3><a style="color:#30E3CA">{{$item->title}}</a></h3>
                  <address>{{$item->address}}</address>
                  <p class="mb-0">
                  <span class="text-warning">{{$item->price}} BDT</span>
                    
                  </p>
                </div>
            
              </div>
        
            </div>
        
            @endforeach
           
            

          </div>










       
           
       @if(Request::path() == 'ads')
       <div class="col-12 mt-5 text-center">
             
        {{ $ads->links() }}
     
      </div>
       @endif


        </div>


        <div class="col-lg-3 ml-auto">

        @include('includes.sideSearch')
         </div>
       





      </div>
    </div>
  </div>














{{-- 
  <div class="site-section bg-light">
    <div class="container">
      <div class="row mb-5">
        <div class="col-md-7 text-left border-primary">
          <h2 class="font-weight-light text-primary">Trending Today</h2>
        </div>
      </div>
      <div class="row mt-5">
        <div class="col-lg-6">

          <div class="d-block d-md-flex listing">
            <a href="#" class="img d-block" style="background-image: url('images/img_2.jpg')"></a>
            <div class="lh-content">
              <span class="category">Real Estate</span>
              <a href="#" class="bookmark"><span class="icon-heart"></span></a>
              <h3><a href="#">House with Swimming Pool</a></h3>
              <address>Don St, Brooklyn, New York</address>
              <p class="mb-0">
                <span class="icon-star text-warning"></span>
                <span class="icon-star text-warning"></span>
                <span class="icon-star text-warning"></span>
                <span class="icon-star text-warning"></span>
                <span class="icon-star text-secondary"></span>
                <span class="review">(3 Reviews)</span>
              </p>
            </div>
          </div>
          <div class="d-block d-md-flex listing">
              <a href="#" class="img d-block" style="background-image: url('images/img_3.jpg')"></a>
              <div class="lh-content">
                <span class="category">Furniture</span>
                <a href="#" class="bookmark"><span class="icon-heart"></span></a>
                <h3><a href="#">Wooden Chair &amp; Table</a></h3>
                <address>Don St, Brooklyn, New York</address>
                <p class="mb-0">
                  <span class="icon-star text-warning"></span>
                  <span class="icon-star text-warning"></span>
                  <span class="icon-star text-warning"></span>
                  <span class="icon-star text-warning"></span>
                  <span class="icon-star text-secondary"></span>
                  <span class="review">(3 Reviews)</span>
                </p>
              </div>
            </div>

            <div class="d-block d-md-flex listing">
              <a href="#" class="img d-block" style="background-image: url('images/img_4.jpg')"></a>
              <div class="lh-content">
                <span class="category">Electronics</span>
                <a href="#" class="bookmark"><span class="icon-heart"></span></a>
                <h3><a href="#">iPhone X gray</a></h3>
                <address>Don St, Brooklyn, New York</address>
                <p class="mb-0">
                  <span class="icon-star text-warning"></span>
                  <span class="icon-star text-warning"></span>
                  <span class="icon-star text-warning"></span>
                  <span class="icon-star text-warning"></span>
                  <span class="icon-star text-secondary"></span>
                  <span class="review">(3 Reviews)</span>
                </p>
              </div>
            </div>

           

        </div>
        <div class="col-lg-6">

          <div class="d-block d-md-flex listing">
            <a href="#" class="img d-block" style="background-image: url('images/img_1.jpg')"></a>
            <div class="lh-content">
              <span class="category">Cars &amp; Vehicles</span>
              <a href="#" class="bookmark"><span class="icon-heart"></span></a>
              <h3><a href="#">Red Luxury Car</a></h3>
              <address>Don St, Brooklyn, New York</address>
              <p class="mb-0">
                <span class="icon-star text-warning"></span>
                <span class="icon-star text-warning"></span>
                <span class="icon-star text-warning"></span>
                <span class="icon-star text-warning"></span>
                <span class="icon-star text-secondary"></span>
                <span class="review">(3 Reviews)</span>
              </p>
            </div>
          </div>

          <div class="d-block d-md-flex listing">
            <a href="#" class="img d-block" style="background-image: url('images/img_2.jpg')"></a>
            <div class="lh-content">
              <span class="category">Real Estate</span>
              <a href="#" class="bookmark"><span class="icon-heart"></span></a>
              <h3><a href="#">House with Swimming Pool</a></h3>
              <address>Don St, Brooklyn, New York</address>
              <p class="mb-0">
                <span class="icon-star text-warning"></span>
                <span class="icon-star text-warning"></span>
                <span class="icon-star text-warning"></span>
                <span class="icon-star text-warning"></span>
                <span class="icon-star text-secondary"></span>
                <span class="review">(3 Reviews)</span>
              </p>
            </div>
          </div>
          <div class="d-block d-md-flex listing">
              <a href="#" class="img d-block" style="background-image: url('images/img_3.jpg')"></a>
              <div class="lh-content">
                <span class="category">Furniture</span>
                <a href="#" class="bookmark"><span class="icon-heart"></span></a>
                <h3><a href="#">Wooden Chair &amp; Table</a></h3>
                <address>Don St, Brooklyn, New York</address>
                <p class="mb-0">
                  <span class="icon-star text-warning"></span>
                  <span class="icon-star text-warning"></span>
                  <span class="icon-star text-warning"></span>
                  <span class="icon-star text-warning"></span>
                  <span class="icon-star text-secondary"></span>
                  <span class="review">(3 Reviews)</span>
                </p>
              </div>
            </div>

        </div>
      </div>
    </div>
  </div>
   --}}


 



  {{-- <script>
    function likes(i,a){
            if (a>1) {
                var j='/likes';
                console.log(a);
            } else {


                var j='/dislikes';
                console.log(a);

            }
            $.ajax({
            method: 'POST', 
            url: j, 
            data: {"_token": "{{ csrf_token() }}",'ads_id':i}, 
            success: function() {   
                location.reload();  
            },
            error: function(jqXHR, textStatus, errorThrown) { 
                console.log(JSON.stringify(jqXHR));
                console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
            }
        });
        }





  </script> --}}
  @endsection
