@extends('master')

@section('content')
  


<div class="site-section">
    <div class="container">
      <div  class="row">
        <div class="col-12">
          <h2 style="font-weight:bold" class="h5 mb-4 text-black">Ads</h2>
        </div>
        <div class="col-lg-12">

          <div style="justify-content: center;
          align-items: center;" class="row">
              
            <div class="media col-lg-8">
                @foreach ($user['media'] as $item)
                <img style="width:300px;height:290px" class="d-flex mr-3" src="{{$item->getUrl()}}" alt="Generic placeholder image">
                @endforeach
                
                <div class="media-body">
                <h2 class="mt-0 font-weight-bold">{{$user->name}}</h2>
                <hr>
                <h5 class="text-dark"><span  class="font-weight-bold text-warning">Address : </span>{{$user->address}}</h5>
                <hr>
                <h5 class="text-dark"><span  class="font-weight-bold text-warning">Email : </span>{{$user->email}}</h5>
                <hr>
                <h5 class="text-dark"><span  class="font-weight-bold text-warning">Phone Number : </span>{{$user->phone_number}}</h5>
                <hr>
                <h5 class="text-dark"><span  class="font-weight-bold text-warning">Blood Group : </span>O+</h5>
                <hr>
                    
                  
                 
                </div>
              </div>
           
            

          </div>

        </div>
        

      </div>
    </div>
  </div>


@endsection