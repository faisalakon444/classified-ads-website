@extends('master')

@section('content')
  


<div class="site-section">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2 style="font-weight:bold" class="h5 mb-4 text-black">Ads</h2>
        </div>
        <div class="col-lg-12">

          <div class="row">
              
            @foreach ($m as $item)
            
            <div  class="col-lg-4">
                
              <div class="d-block d-md-flex listing vertical">
              {{-- <a href="{{url('/adsdetails'.$item['id'])}}" class="img d-block" style="background-image:url('{{$item['media'][0]->getUrl()}}')"></a> --}}
                <div  class="lh-content">
                  <span class="category">{{$item['sender'][0]->name}}</span>
                 
                  <h3>Contact :<a style="color:#30E3CA"> {{$item['sender'][0]->phone_number}}</a></h3>
                  <address>{{$item->message}}</address>
                  <p class="mb-0">
                  {{-- <span class="text-warning">{{$item->}} BDT</span> --}}
                  <a class="btn btn-danger" href="{{url('/message-delete'.$item['id'] )}}" onclick="return confirm('Are you sure?')">Delete</a>
                  <a class="btn btn-info" href="{{url('/adsdetails'.$item['ads_id'] )}}" onclick="return confirm('Do you wanna visite that ad?')">Ad's Link</a>

                  </p>
                </div>
            
              </div>
        
            </div>
        
            @endforeach
           
            

          </div>












          <div class="col-12 mt-5 text-center">
            {{-- <div class="custom-pagination"> --}}
              {{-- {{ $ads->links() }} --}}
            {{-- </div> --}}
          </div>

        </div>
        

      </div>
    </div>
  </div>


@endsection