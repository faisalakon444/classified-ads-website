
@extends('master')

@section('content')
  

<div class="site-blocks-cover inner-page-cover overlay" style="background-image: url(images/hero_1.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
    <div class="container">
      <div class="row align-items-center justify-content-center text-center">

        <div class="col-md-10" data-aos="fade-up" data-aos-delay="400">
          
          
          <div class="row justify-content-center mt-5">
            <div class="col-md-8 text-center">
              <h1>Ads Listings</h1>
              <p class="mb-0">Choose product you want</p>
            </div>
          </div>

          
        </div>
      </div>
    </div>
  </div>  

  <div class="site-section">
    <div class="container">
      <div class="row">
        <div class="col-lg-8">

          <div class="row">
              @foreach ($ads as $item)
              
            <div  class="col-lg-6">
                
              <div class="d-block d-md-flex listing vertical">
                @foreach ($item['media'] as $img)
                <a href="{{url('/adsdetails'.$item['id'])}}" class="img d-block" style="background-image:url('{{$img->getUrl()}}')"></a>
                @break
                @endforeach
              
             
                <div  class="lh-content">
                  <span class="category">{{$item['section'][0]->section_name}}</span>
 
                  <h3><a style="color:#30E3CA">{{$item->title}}</a></h3>
                  <address>{{$item->address}},{{$item['location'][0]->location_name}}</address>
                  <p class="mb-0">
                  <span class="text-warning">{{$item->price}} BDT</span>
                    
                  </p>
                  <a class="btn btn-primary" href="{{url('/ads-edit'.$item['id'])}}">Edit</a>
                  <a class="btn btn-danger" href="{{url('/ads-delete'.$item['id'] )}}" onclick="return confirm('Are you sure?')">Delete</a>
                </div>
            
              </div>
        
            </div>
        
            @endforeach
           
          

          </div>

          @if(Request::path() == 'userads')
          <div class="col-12 mt-5 text-center">
                
           {{ $ads->links() }}
        
         </div>
          @endif

        </div>
        <div class="col-lg-3 ml-auto">
        @include('includes.sideSearch')
        </div>







      </div>
    </div>
  </div>

   

   <div class="site-section">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2 style="font-weight:bold" class="h5 mb-4 text-black">Ads</h2>
        </div>
        <div class="col-lg-12">

          <div class="row">
              @foreach ($adsAll as $item)
              
            <div  class="col-lg-4">
                
              <div class="d-block d-md-flex listing vertical">
              <a href="{{url('/adsdetails'.$item['id'])}}" class="img d-block" style="background-image:url('{{$item['media'][0]->getUrl()}}')"></a>
                <div  class="lh-content">
                  <span class="category">{{$item['section'][0]->section_name}}</span>
                  
                  <h3><a style="color:#30E3CA">{{$item->title}}</a></h3>
                  <address>{{$item->address}},{{$item['location'][0]->location_name}}</address>
                  <p class="mb-0">
                  <span class="text-warning">{{$item->price}} BDT</span>
                    
                  </p>
                </div>
            
              </div>
        
            </div>
        
            @endforeach
           
            

          </div>



          {{-- <div class="col-12 mt-5 text-center">
           
              {{ $adsAll->links() }}
           
          </div> --}}

        </div>
        

      </div>
    </div>
  </div>

  
  @endsection
