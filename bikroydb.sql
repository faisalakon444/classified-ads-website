-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 05, 2020 at 09:35 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bikroydb`
--

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE `ads` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `section_id` tinyint(4) NOT NULL,
  `specification` text COLLATE utf8mb4_unicode_ci,
  `price` int(11) NOT NULL,
  `location_id` tinyint(4) DEFAULT NULL,
  `user_id` tinyint(4) NOT NULL,
  `like_count` int(11) NOT NULL DEFAULT '0',
  `address` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `publish` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ads`
--

INSERT INTO `ads` (`id`, `title`, `section_id`, `specification`, `price`, `location_id`, `user_id`, `like_count`, `address`, `created_at`, `updated_at`, `publish`) VALUES
(25, 'Table', 2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam hendrerit nisi sed sollicitudin pellentesque. Nunc posuere purus rhoncus pulvinar aliquam. Ut aliquet tristique nisl vitae volutpat. Nulla aliquet porttitor venenatis. Donec a dui et dui f', 12000, 3, 12, 0, 'Mogbazar,chanbekary goli', '2020-03-31 02:17:22', '2020-04-01 14:51:15', 1),
(26, 'car', 3, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English', 1000000, 1, 11, 0, 'Mohanorgor project', '2020-03-31 03:44:05', '2020-04-01 14:53:19', 1),
(27, 'Laptop', 1, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English', 250000, 2, 11, 0, 'chingri chottor', '2020-03-31 03:45:57', '2020-03-31 04:02:17', 1),
(28, 'Bird', 5, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English', 25000, 5, 11, 0, 'gollamari', '2020-03-31 03:47:33', '2020-04-01 14:26:29', 1),
(29, 'House', 4, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English', 5000000, 8, 12, 0, 'desko shingpo', '2020-03-31 03:50:01', '2020-04-01 14:26:25', 1),
(30, 'Motorcycle', 3, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English', 657432, 6, 12, 0, 'someshor', '2020-03-31 03:56:48', '2020-04-01 14:47:14', 1),
(31, 'Motorcycle', 3, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English', 657432, 6, 12, 0, 'someshor', '2020-03-31 03:56:59', '2020-03-31 04:02:03', 1),
(32, 'Motorcycle', 3, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English', 657432, 6, 12, 0, 'someshor', '2020-03-31 03:57:03', '2020-03-31 05:19:35', 1),
(33, 'telescope', 6, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English', 123456, 7, 12, 0, 'hochuku', '2020-03-31 04:00:17', '2020-03-31 04:02:22', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bloods`
--

CREATE TABLE `bloods` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `groups` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bloods`
--

INSERT INTO `bloods` (`id`, `groups`, `created_at`, `updated_at`) VALUES
(1, 'A+', NULL, NULL),
(2, 'A-', NULL, NULL),
(3, 'O+', NULL, NULL),
(4, 'O-', NULL, NULL),
(5, 'AB-', NULL, NULL),
(6, 'AB+', NULL, NULL),
(7, 'B+', NULL, NULL),
(8, 'B-', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ads_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `ads_id`, `user_id`, `comment`, `created_at`, `updated_at`) VALUES
(1, 22, 12, 'bikroy.com faul, faltu product', '2020-03-28 05:01:05', '2020-03-28 05:01:05'),
(2, 22, 12, 'bikroy.com faul, faltu product', '2020-03-28 05:01:07', '2020-03-28 05:01:07'),
(3, 22, 12, 'bikroy.com faul, faltu product', '2020-03-28 05:01:07', '2020-03-28 05:01:07'),
(4, 22, 12, 'bikroy.com faul, faltu product', '2020-03-28 05:01:08', '2020-03-28 05:01:08'),
(5, 22, 12, 'bikroy.com faul, faltu product', '2020-03-28 05:01:09', '2020-03-28 05:01:09'),
(6, 13, 1, 'this is good', '2020-03-28 13:21:12', '2020-03-28 13:21:12'),
(7, 19, 1, 'valo to valo na ?', '2020-03-29 09:18:29', '2020-03-29 09:18:29'),
(8, 17, 1, 'hello', '2020-03-30 01:20:39', '2020-03-30 01:20:39'),
(9, 25, 11, 'I wanna buy', '2020-03-31 04:09:07', '2020-03-31 04:09:07'),
(10, 26, 13, 'valo', '2020-03-31 05:17:45', '2020-03-31 05:17:45');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `genders`
--

CREATE TABLE `genders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sex` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `genders`
--

INSERT INTO `genders` (`id`, `sex`, `created_at`, `updated_at`) VALUES
(1, 'Male', NULL, NULL),
(2, 'Female', NULL, NULL),
(3, 'Others', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `ads_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `user_id`, `ads_id`, `created_at`, `updated_at`) VALUES
(9, 12, 14, '2020-03-28 04:33:14', '2020-03-28 04:33:14'),
(10, 12, 13, '2020-03-28 04:34:57', '2020-03-28 04:34:57'),
(11, 12, 8, '2020-03-28 04:35:08', '2020-03-28 04:35:08'),
(12, 1, 13, '2020-03-28 13:20:46', '2020-03-28 13:20:46');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `location_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `location_name`, `created_at`, `updated_at`) VALUES
(1, 'Rajshahi', NULL, NULL),
(2, 'Khulna', NULL, NULL),
(3, 'Dhaka', NULL, NULL),
(4, 'Barishal', NULL, NULL),
(5, 'Maymansingh', NULL, NULL),
(6, 'Chattogram', NULL, NULL),
(7, 'Rangpur', NULL, NULL),
(8, 'Sylhet', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  `collection_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` bigint(20) UNSIGNED NOT NULL,
  `manipulations` json NOT NULL,
  `custom_properties` json NOT NULL,
  `responsive_images` json NOT NULL,
  `order_column` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `model_type`, `model_id`, `collection_name`, `name`, `file_name`, `mime_type`, `disk`, `size`, `manipulations`, `custom_properties`, `responsive_images`, `order_column`, `created_at`, `updated_at`) VALUES
(41, 'App\\Ads', 25, 'default', 'download (1)', 'download-(1).jfif', 'image/jpeg', 'public', 2931, '[]', '[]', '[]', 35, '2020-03-31 02:17:22', '2020-03-31 02:17:22'),
(42, 'App\\Ads', 25, 'default', 'download (2)', 'download-(2).jfif', 'image/jpeg', 'public', 3098, '[]', '[]', '[]', 36, '2020-03-31 02:17:22', '2020-03-31 02:17:22'),
(43, 'App\\Ads', 25, 'default', 'download (3)', 'download-(3).jfif', 'image/jpeg', 'public', 2111, '[]', '[]', '[]', 37, '2020-03-31 02:17:22', '2020-03-31 02:17:22'),
(44, 'App\\Ads', 25, 'default', 'download', 'download.jfif', 'image/jpeg', 'public', 1831, '[]', '[]', '[]', 38, '2020-03-31 02:17:22', '2020-03-31 02:17:22'),
(45, 'App\\Ads', 25, 'default', 'download', 'download.png', 'image/png', 'public', 1163, '[]', '[]', '[]', 39, '2020-03-31 02:17:22', '2020-03-31 02:17:22'),
(48, 'App\\Ads', 26, 'default', 'download (6)', 'download-(6).jfif', 'image/jpeg', 'public', 2929, '[]', '[]', '[]', 40, '2020-03-31 03:44:05', '2020-03-31 03:44:05'),
(49, 'App\\Ads', 26, 'default', 'download (7)', 'download-(7).jfif', 'image/jpeg', 'public', 2136, '[]', '[]', '[]', 41, '2020-03-31 03:44:05', '2020-03-31 03:44:05'),
(50, 'App\\Ads', 26, 'default', 'download (8)', 'download-(8).jfif', 'image/jpeg', 'public', 2384, '[]', '[]', '[]', 42, '2020-03-31 03:44:05', '2020-03-31 03:44:05'),
(51, 'App\\Ads', 26, 'default', 'download (9)', 'download-(9).jfif', 'image/jpeg', 'public', 2243, '[]', '[]', '[]', 43, '2020-03-31 03:44:05', '2020-03-31 03:44:05'),
(52, 'App\\Ads', 26, 'default', 'download (10)', 'download-(10).jfif', 'image/jpeg', 'public', 3413, '[]', '[]', '[]', 44, '2020-03-31 03:44:05', '2020-03-31 03:44:05'),
(53, 'App\\Ads', 27, 'default', 'download (1)', 'download-(1).jfif', 'image/jpeg', 'public', 2391, '[]', '[]', '[]', 45, '2020-03-31 03:45:57', '2020-03-31 03:45:57'),
(54, 'App\\Ads', 27, 'default', 'download (2)', 'download-(2).jfif', 'image/jpeg', 'public', 1788, '[]', '[]', '[]', 46, '2020-03-31 03:45:57', '2020-03-31 03:45:57'),
(55, 'App\\Ads', 27, 'default', 'download (3)', 'download-(3).jfif', 'image/jpeg', 'public', 1827, '[]', '[]', '[]', 47, '2020-03-31 03:45:57', '2020-03-31 03:45:57'),
(56, 'App\\Ads', 27, 'default', 'download (4)', 'download-(4).jfif', 'image/jpeg', 'public', 1871, '[]', '[]', '[]', 48, '2020-03-31 03:45:57', '2020-03-31 03:45:57'),
(57, 'App\\Ads', 27, 'default', 'download', 'download.jfif', 'image/jpeg', 'public', 1834, '[]', '[]', '[]', 49, '2020-03-31 03:45:57', '2020-03-31 03:45:57'),
(58, 'App\\Ads', 28, 'default', 'download (20)', 'download-(20).jfif', 'image/jpeg', 'public', 3254, '[]', '[]', '[]', 50, '2020-03-31 03:47:33', '2020-03-31 03:47:33'),
(59, 'App\\Ads', 28, 'default', 'download (21)', 'download-(21).jfif', 'image/jpeg', 'public', 2897, '[]', '[]', '[]', 51, '2020-03-31 03:47:33', '2020-03-31 03:47:33'),
(60, 'App\\Ads', 28, 'default', 'download (22)', 'download-(22).jfif', 'image/jpeg', 'public', 2270, '[]', '[]', '[]', 52, '2020-03-31 03:47:33', '2020-03-31 03:47:33'),
(61, 'App\\Ads', 28, 'default', 'download (23)', 'download-(23).jfif', 'image/jpeg', 'public', 1244, '[]', '[]', '[]', 53, '2020-03-31 03:47:33', '2020-03-31 03:47:33'),
(62, 'App\\Ads', 28, 'default', 'images (9)', 'images-(9).jfif', 'image/jpeg', 'public', 2079, '[]', '[]', '[]', 54, '2020-03-31 03:47:33', '2020-03-31 03:47:33'),
(63, 'App\\Ads', 29, 'default', 'images (5)', 'images-(5).jfif', 'image/jpeg', 'public', 3613, '[]', '[]', '[]', 55, '2020-03-31 03:50:01', '2020-03-31 03:50:01'),
(64, 'App\\Ads', 30, 'default', 'download (1)', 'download-(1).jfif', 'image/jpeg', 'public', 3158, '[]', '[]', '[]', 56, '2020-03-31 03:56:49', '2020-03-31 03:56:49'),
(65, 'App\\Ads', 30, 'default', 'download (2)', 'download-(2).jfif', 'image/jpeg', 'public', 3200, '[]', '[]', '[]', 57, '2020-03-31 03:56:49', '2020-03-31 03:56:49'),
(66, 'App\\Ads', 30, 'default', 'download (3)', 'download-(3).jfif', 'image/jpeg', 'public', 2635, '[]', '[]', '[]', 58, '2020-03-31 03:56:49', '2020-03-31 03:56:49'),
(67, 'App\\Ads', 30, 'default', 'download (4)', 'download-(4).jfif', 'image/jpeg', 'public', 3731, '[]', '[]', '[]', 59, '2020-03-31 03:56:49', '2020-03-31 03:56:49'),
(68, 'App\\Ads', 30, 'default', 'download (5)', 'download-(5).jfif', 'image/jpeg', 'public', 3325, '[]', '[]', '[]', 60, '2020-03-31 03:56:49', '2020-03-31 03:56:49'),
(69, 'App\\Ads', 31, 'default', 'download (1)', 'download-(1).jfif', 'image/jpeg', 'public', 3158, '[]', '[]', '[]', 61, '2020-03-31 03:56:59', '2020-03-31 03:56:59'),
(70, 'App\\Ads', 31, 'default', 'download (2)', 'download-(2).jfif', 'image/jpeg', 'public', 3200, '[]', '[]', '[]', 62, '2020-03-31 03:56:59', '2020-03-31 03:56:59'),
(71, 'App\\Ads', 31, 'default', 'download (3)', 'download-(3).jfif', 'image/jpeg', 'public', 2635, '[]', '[]', '[]', 63, '2020-03-31 03:56:59', '2020-03-31 03:56:59'),
(72, 'App\\Ads', 31, 'default', 'download (4)', 'download-(4).jfif', 'image/jpeg', 'public', 3731, '[]', '[]', '[]', 64, '2020-03-31 03:56:59', '2020-03-31 03:56:59'),
(73, 'App\\Ads', 31, 'default', 'download (5)', 'download-(5).jfif', 'image/jpeg', 'public', 3325, '[]', '[]', '[]', 65, '2020-03-31 03:56:59', '2020-03-31 03:56:59'),
(74, 'App\\Ads', 32, 'default', 'download (1)', 'download-(1).jfif', 'image/jpeg', 'public', 3158, '[]', '[]', '[]', 66, '2020-03-31 03:57:03', '2020-03-31 03:57:03'),
(75, 'App\\Ads', 32, 'default', 'download (2)', 'download-(2).jfif', 'image/jpeg', 'public', 3200, '[]', '[]', '[]', 67, '2020-03-31 03:57:03', '2020-03-31 03:57:03'),
(76, 'App\\Ads', 32, 'default', 'download (3)', 'download-(3).jfif', 'image/jpeg', 'public', 2635, '[]', '[]', '[]', 68, '2020-03-31 03:57:04', '2020-03-31 03:57:04'),
(77, 'App\\Ads', 32, 'default', 'download (4)', 'download-(4).jfif', 'image/jpeg', 'public', 3731, '[]', '[]', '[]', 69, '2020-03-31 03:57:04', '2020-03-31 03:57:04'),
(78, 'App\\Ads', 32, 'default', 'download (5)', 'download-(5).jfif', 'image/jpeg', 'public', 3325, '[]', '[]', '[]', 70, '2020-03-31 03:57:04', '2020-03-31 03:57:04'),
(79, 'App\\Ads', 33, 'default', 'download (24)', 'download-(24).jfif', 'image/jpeg', 'public', 6935, '[]', '[]', '[]', 71, '2020-03-31 04:00:17', '2020-03-31 04:00:17'),
(80, 'App\\Ads', 33, 'default', 'download (25)', 'download-(25).jfif', 'image/jpeg', 'public', 7513, '[]', '[]', '[]', 72, '2020-03-31 04:00:17', '2020-03-31 04:00:17'),
(81, 'App\\Ads', 33, 'default', 'download (26)', 'download-(26).jfif', 'image/jpeg', 'public', 5520, '[]', '[]', '[]', 73, '2020-03-31 04:00:17', '2020-03-31 04:00:17'),
(82, 'App\\Ads', 33, 'default', 'download (27)', 'download-(27).jfif', 'image/jpeg', 'public', 5851, '[]', '[]', '[]', 74, '2020-03-31 04:00:17', '2020-03-31 04:00:17'),
(83, 'App\\Ads', 33, 'default', 'download (28)', 'download-(28).jfif', 'image/jpeg', 'public', 4491, '[]', '[]', '[]', 75, '2020-03-31 04:00:17', '2020-03-31 04:00:17'),
(84, 'App\\User', 12, 'default', 'SZ6SrEV8_400x400', 'SZ6SrEV8_400x400.jpg', 'image/jpeg', 'public', 29461, '[]', '[]', '[]', 76, '2020-03-31 04:22:27', '2020-03-31 04:22:27'),
(85, 'App\\User', 11, 'default', 'FB_IMG_15370017694825926', 'FB_IMG_15370017694825926.jpg', 'image/jpeg', 'public', 218420, '[]', '[]', '[]', 77, '2020-03-31 04:26:11', '2020-03-31 04:26:11'),
(86, 'App\\User', 13, 'default', 'a44af3bb5f074e3cdb4be8a56232c996', 'a44af3bb5f074e3cdb4be8a56232c996.jpg', 'image/jpeg', 'public', 67998, '[]', '[]', '[]', 78, '2020-03-31 04:28:30', '2020-03-31 04:28:30'),
(87, 'App\\User', 1, 'default', 'hiro-hamada-blue-hoodie-750x750', 'hiro-hamada-blue-hoodie-750x750.jpg', 'image/jpeg', 'public', 69137, '[]', '[]', '[]', 79, '2020-03-31 04:35:02', '2020-03-31 04:35:02');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `from` int(11) NOT NULL,
  `to` int(11) NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `seen` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ads_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `from`, `to`, `message`, `seen`, `created_at`, `updated_at`, `ads_id`) VALUES
(7, 11, 12, 'Nice Table', 0, '2020-03-31 04:06:57', '2020-03-31 04:06:57', 25),
(8, 12, 11, 'nice car', 0, '2020-03-31 04:08:04', '2020-03-31 04:08:04', 26),
(9, 13, 11, 'ami kinte chai .', 0, '2020-03-31 05:17:02', '2020-03-31 05:17:02', 26);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_03_24_034235_create_permission_tables', 2),
(5, '2020_03_24_095918_create_media_table', 3),
(9, '2020_03_24_181914_create_bloods_table', 4),
(10, '2020_03_24_182011_create_genders_table', 4),
(11, '2020_03_24_182052_create_locations_table', 4),
(12, '2020_03_25_071001_create_ads_table', 5),
(13, '2020_03_25_072627_create_sections_table', 5),
(16, '2020_03_27_101809_create_likes_table', 6),
(17, '2020_03_27_102042_create_comments_table', 6),
(18, '2020_03_29_112757_add_publish_to_ads', 7),
(19, '2020_03_30_051955_create_messages_table', 8),
(20, '2020_03_30_201815_add_adsid_to_messages', 9);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(3, 'App\\User', 1),
(2, 'App\\User', 11),
(2, 'App\\User', 12),
(1, 'App\\User', 13);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'ads_post', 'web', '2020-03-24 23:16:08', '2020-03-24 23:16:08');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'user', 'web', '2020-03-24 23:16:07', '2020-03-24 23:16:07'),
(2, 'advertiser', 'web', '2020-03-24 23:16:07', '2020-03-24 23:16:07'),
(3, 'admin', 'web', '2020-03-29 00:52:41', '2020-03-29 00:52:41');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `section_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`id`, `section_name`, `created_at`, `updated_at`) VALUES
(1, 'Electronics', NULL, NULL),
(2, 'Home Appliance ', NULL, NULL),
(3, 'Vehicles', NULL, NULL),
(4, 'Property', NULL, NULL),
(5, 'Pets', NULL, NULL),
(6, 'Hobbies', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  `age` tinyint(4) DEFAULT NULL,
  `blood_id` tinyint(4) DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` tinyint(4) DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `gender`, `age`, `blood_id`, `phone_number`, `location`, `address`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Faisal', 'emailforadmin@gmail.com', NULL, '$2y$10$z1dWLjpsqH/uCc47/ifaqej0woMc7wa/fiVf18cUyaehiYP5Fg.Ta', 1, NULL, 3, '01866233455', 3, 'mohanogor project', NULL, '2020-03-23 00:51:58', '2020-03-24 13:21:48'),
(11, 'Shuvo', 'aemailforshuvo@gmail.com', NULL, '$2y$10$X/IIQbrGea2qIwgOrWibO.SPCNqy3yUWE3Ihh.91NGd2Akksozs9m', 1, NULL, 1, '7777777772126', 1, NULL, NULL, '2020-03-25 00:30:56', '2020-03-31 04:23:41'),
(12, 'Pial', 'aemailforpial@gmail.com', NULL, '$2y$10$4YBxaPCTjqcbw/XCpw5NlecZAKREoTDibjnHJOsia4psuLzuZDPBK', 1, NULL, 3, '0187132813687136736', 3, 'Mohanogor', NULL, '2020-03-25 00:36:32', '2020-03-25 01:05:37'),
(13, 'tarek', 'aemailfortarek@t.com', NULL, '$2y$10$Tzc0/dz0FMi6.LbgSWplh.7qaxjXbWbr1YEseZ/0nA47e5APMkwhC', 1, NULL, 1, '44556677889', 1, NULL, NULL, '2020-03-30 22:17:43', '2020-03-31 04:28:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bloods`
--
ALTER TABLE `bloods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `genders`
--
ALTER TABLE `genders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_phone_number_unique` (`phone_number`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ads`
--
ALTER TABLE `ads`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `bloods`
--
ALTER TABLE `bloods`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `genders`
--
ALTER TABLE `genders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
